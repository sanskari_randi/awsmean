var express = require("express");
var bodyparser = require("body-parser");

var app = express();

var mongoose = require("mongoose");

var db  = 'mongodb://localhost:27017/awsmean';


app.use(bodyparser.urlencoded({extended:true}));
app.use(bodyparser.json());



var Schema = mongoose.Schema;

var nameSchema = new Schema({

	firstname: String,
	lastname: String
}); 

var Name = mongoose.model('Name', nameSchema);

var router = express.Router();



router.route('/')
	.post(function(req,res,next){
	console.log(req.body);
	var name = new Name();
		
		
	name.firstname = req.body.firstname;
	name.lastname = req.body.lastname;
	

	name.save(function(err){
	
		if(err){
		res.send(err);
		next();
		}
		res.json({message: "name saved", name:name});
	});

	})

	.get(function(req,res){
	
		Name.find({},function(err,names){
		
			if(err){
				res.send(err);		
				next();
				}
			res.json(names);		
		});
	});


mongoose.connect(db, function(err){

	if(err){
	console.log(err);
	}
	console.log("connected to db");

});

app.use(router);
app.listen(3000, function(err){

	if(err){
	console.log(err);
	}
	console.log("server listening on port 3000");
});
